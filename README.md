# README #

Simulation of a Back-End in a client-server architecture - client injecting a message and a server listening and returning a reply. Query/reply are in Edifact format well known in travel industry standard.

### What is this repository for? ###

Purpose is to simulate a back-end appli using the suite Kubernetes/POD/Docker and play with it by injecting traffic. Objective is to run locally on MacBook and on Google Cloud Engine.

### How do I get set up? ###

* Base Docker image is Centos y/Python 2.7. I added one single file in home directory: server.py who is started automatically when the Docker containing is running. Server is listening on port 33333. Client (File client.py is the client to use)
Docker image stored on Dockerhub: denice/pythonclientserver:v1

* The server is returning a unique Id allowing to identify which POD & container is answering the query. This is good to play killing Back-ends and check the recovery.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines
All to be done